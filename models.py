from sqlmodel import Field, Session, SQLModel, select
from db import get_db_engine


class Employee(SQLModel, table=True):
    __tablename__ = 'employees'
    id: int = Field(primary_key=True)
    gender: str


def select_employees():
    with Session(get_db_engine()) as session:
        statement = select(Employee)
        results = session.exec(statement)
        # @todo exiting the `with` closes the connection, so we have to load the results here.
        #  might be worth making this lazier.
        return results.all()
