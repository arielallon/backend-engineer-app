# Backend App

## Getting Up and Running

(This guide assumes you have `python3` and `git` installed.)

Define what port the server will use as an environmental variable. For example:
```shell
export PORT=7891
```

Create a directory you'd like to run this from, then navigate to that directory.
For example:
```shell
mkdir ~/backend-app
cd ~/backend-app
```

Download this repository:
```shell
git clone git@gitlab.com:arielallon/backend-engineer-app.git ./
```

(Optional but recommended: set up a virtualenv
```shell
python3 -m venv venv
. venv/bin/activate
```
)

Install python dependencies
```shell
pip3 install -r requirements.txt
```

Start the server:
```shell
uvicorn main:app --port $PORT
```

Note: if you run into an error like `ModuleNotFoundError: No module named 'sqlmodel'`, 
you may need to double-check that the virtualenv was properly activated.
Re-running `. venv/bin/activate` should be sufficient to then run `uvicorn main:app --port $PORT` successfully.

## Querying the API

You can query the `employees` endpoint from the CLI via `curl localhost:$PORT/employees`.

You can also point a browser or API tool (e.g. Postman) at the same URL, 
though you'll need to be explicit about the port in those contexts.


----
Data is sourced from https://github.com/syndio/backend-engineer-app