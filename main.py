from fastapi import FastAPI
from models import select_employees

app = FastAPI()

@app.get("/employees")
def get_employees():
    # @todo consider limitations on how many records can be returned and allow for pagination
    employees = select_employees()
    return employees