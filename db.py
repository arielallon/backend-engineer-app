from sqlmodel import create_engine

# @todo this may prevent the use of a "singleton" engine. revisit.
def get_db_engine():
    sqlite_filename = "employees.db"
    sqlite_url = f"sqlite:///{sqlite_filename}"
    engine = create_engine(sqlite_url)
    return engine